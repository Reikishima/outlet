import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'schedule.g.dart';

List<ResponseModel> userModelFromMap(String str) =>
    List<ResponseModel>.from(json.decode(str).map((x) => ResponseModel.fromJson(x)));

String userModelToMap(List<ResponseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

@JsonSerializable()
class ResponseModel {
  ResponseModel({
    required this.page,
    required this.perPage,
    required this.total,
    required this.totalPages,
    required this.data,
    required this.support,
  });
  @JsonKey(name: 'page')
  final int? page;
  @JsonKey(name: 'per_page')
  final int? perPage;
  @JsonKey(name: 'total')
  final int? total;
  @JsonKey(name: 'total_pages')
  final int? totalPages;
  @JsonKey(name: 'data')
  final List<DataReq?> data;
  @JsonKey(name: 'support')
  final Support? support;

  factory ResponseModel.fromJson(Map<String, dynamic> json) =>
      _$ResponseModelFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseModelToJson(this);
}

@JsonSerializable()
class DataReq {
  DataReq({
    required this.id,
    required this.email,
    required this.firstName,
    required this.lastName,
    required this.avatar,
  });
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'email')
  final String? email;
  @JsonKey(name: 'first_name')
  final String? firstName;
  @JsonKey(name: 'last_name')
  final String? lastName;
  @JsonKey(name: 'avatar')
  final String? avatar;

  factory DataReq.fromJson(Map<String, dynamic> json) => _$DataReqFromJson(json);

  Map<String, dynamic> toJson() => _$DataReqToJson(this);
}

@JsonSerializable()
class Support {
  Support({
    required this.url,
    required this.text,
  });
  @JsonKey(name: 'url')
  final String? url;
  @JsonKey(name: 'text')
  final String? text;

  factory Support.fromJson(Map<String, dynamic> json) =>
      _$SupportFromJson(json);

  Map<String, dynamic> toJson() => _$SupportToJson(this);
}
