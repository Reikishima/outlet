import 'dart:convert';

import 'package:flutter/src/material/data_table.dart';
import 'package:json_annotation/json_annotation.dart';

part 'outlet_details_model.g.dart';

List<OutletDetail> userModelFromMap(String str) => List<OutletDetail>.from(
    json.decode(str).map((x) => OutletDetail.fromJson(x)));

String userModelToMap(List<OutletDetail> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

@JsonSerializable()
class OutletDetail {
  OutletDetail({
    required this.code,
    required this.message,
    required this.dataDetail,
  });

  @JsonKey(name: 'code')
  final int code;
  @JsonKey(name: 'message')
  final String message;
  @JsonKey(name: 'data')
  final DataDetail? dataDetail;

  factory OutletDetail.fromJson(Map<String, dynamic> json) =>_$OutletDetailFromJson(json);

  Map<String, dynamic> toJson() => _$OutletDetailToJson(this);
}

@JsonSerializable()
class DataDetail {
  DataDetail({
    required this.id,
    required this.image,
    required this.name,
    required this.openAtDay,
    required this.openAtTime,
    required this.statusName,
    required this.statusDisabled,
    required this.phoneNumber,
    required this.address,
    required this.googleMapsUrl,
    required this.operationalHour,
  });

  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'image')
  final String image;
  @JsonKey(name: 'name')
  final String name;
  @JsonKey(name: 'open_at_day')
  final String openAtDay;
  @JsonKey(name: 'open_at_time')
  final String openAtTime;
  @JsonKey(name: 'status_name')
  final String statusName;
  @JsonKey(name: 'status_disabled')
  final int statusDisabled;
  @JsonKey(name: 'phone_number')
  final String phoneNumber;
  @JsonKey(name: 'address')
  final String address;
  @JsonKey(name: 'google_maps_url')
  final String googleMapsUrl;
  @JsonKey(name: 'operational_hour')
  final List<OperationalHour> operationalHour;

  factory DataDetail.fromJson(Map<String, dynamic> json) =>_$DataDetailFromJson(json);

  Map<String, dynamic> toJson() => _$DataDetailToJson(this);
}

@JsonSerializable()
class OperationalHour {
  OperationalHour({
    required this.openDay,
    required this.openTime,
  });

  @JsonKey(name: 'open_day')
  final String openDay;
  @JsonKey(name: 'open_time')
  final String openTime;

  factory OperationalHour.fromJson(Map<String, dynamic> json) =>_$OperationalHourFromJson(json);

  Map<String, dynamic> toJson() => _$OperationalHourToJson(this);

  map(DataCell Function(dynamic e) param0) {}
}
