import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';

part 'outlet_list.g.dart';

List<OutletModel> userModelFromMap(String str) =>
    List<OutletModel>.from(json.decode(str).map((x) => OutletModel.fromJson(x)));

String userModelToMap(List<OutletModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

@JsonSerializable()
class OutletModel {
  OutletModel({
    required this.code,
    required this.message,
    required this.data,
    required this.meta,
  });
  @JsonKey(name: 'code')
  final int? code;
  @JsonKey(name: 'message')
  final String? message;
  @JsonKey(name: 'data')
  final List<DataOutlet?> data;
  @JsonKey(name: 'meta')
  final Meta? meta;

  factory OutletModel.fromJson(Map<String, dynamic> json) =>
      _$OutletModelFromJson(json);

  Map<String, dynamic> toJson() => _$OutletModelToJson(this);
}

@JsonSerializable()
class DataOutlet {
  DataOutlet({
    required this.id,
    required this.imageThumb,
    required this.name,
    required this.openAtDisplay,
    required this.statusName,
    required this.statusDisabled,
  });
  @JsonKey(name: 'id')
  final int? id;
  @JsonKey(name: 'image_thumb')
  final String? imageThumb;
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'open_at_display')
  final String? openAtDisplay;
  @JsonKey(name: 'status_name')
  final String? statusName;
  @JsonKey(name: 'status_disabled')
  final int? statusDisabled;

  factory DataOutlet.fromJson(Map<String, dynamic> json) => _$DataOutletFromJson(json);

  Map<String, dynamic> toJson() => _$DataOutletToJson(this);
}

@JsonSerializable()
class Meta {
  Meta({
    required this.page,
    required this.perPage,
    required this.count,
    required this.totalPage,
    required this.totalCount,
  });
  @JsonKey(name: 'page')
  final int? page;
  @JsonKey(name: 'per_page')
  final int? perPage;
  @JsonKey(name: 'count')
  final int? count;
  @JsonKey(name: 'total_page')
  final int? totalPage;
  @JsonKey(name: 'total_count')
  final int? totalCount;

  factory Meta.fromJson(Map<String, dynamic> json) => _$MetaFromJson(json);

  Map<String, dynamic> toJson() => _$MetaToJson(this);
}
