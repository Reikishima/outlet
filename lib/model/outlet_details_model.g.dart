// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'outlet_details_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OutletDetail _$OutletDetailFromJson(Map<String, dynamic> json) => OutletDetail(
      code: json['code'] as int,
      message: json['message'] as String,
      dataDetail: json['data'] == null
          ? null
          : DataDetail.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$OutletDetailToJson(OutletDetail instance) =>
    <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'data': instance.dataDetail,
    };

DataDetail _$DataDetailFromJson(Map<String, dynamic> json) => DataDetail(
      id: json['id'] as int?,
      image: json['image'] as String,
      name: json['name'] as String,
      openAtDay: json['open_at_day'] as String,
      openAtTime: json['open_at_time'] as String,
      statusName: json['status_name'] as String,
      statusDisabled: json['status_disabled'] as int,
      phoneNumber: json['phone_number'] as String,
      address: json['address'] as String,
      googleMapsUrl: json['google_maps_url'] as String,
      operationalHour: (json['operational_hour'] as List<dynamic>)
          .map((e) => OperationalHour.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$DataDetailToJson(DataDetail instance) =>
    <String, dynamic>{
      'id': instance.id,
      'image': instance.image,
      'name': instance.name,
      'open_at_day': instance.openAtDay,
      'open_at_time': instance.openAtTime,
      'status_name': instance.statusName,
      'status_disabled': instance.statusDisabled,
      'phone_number': instance.phoneNumber,
      'address': instance.address,
      'google_maps_url': instance.googleMapsUrl,
      'operational_hour': instance.operationalHour,
    };

OperationalHour _$OperationalHourFromJson(Map<String, dynamic> json) =>
    OperationalHour(
      openDay: json['open_day'] as String,
      openTime: json['open_time'] as String,
    );

Map<String, dynamic> _$OperationalHourToJson(OperationalHour instance) =>
    <String, dynamic>{
      'open_day': instance.openDay,
      'open_time': instance.openTime,
    };
