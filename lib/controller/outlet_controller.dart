import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
// import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:uiprojectbo/model/outlet_details_model.dart';
import 'package:uiprojectbo/model/outlet_list.dart';
import 'package:uiprojectbo/repository/repository_outlet.dart';
import 'package:uiprojectbo/service/outlet_helper.dart';

class OutletController extends GetxController {
  // ? initiation variable
  var isLoading = true.obs;
  var isError = false.obs;
  RxString errorMessage = ''.obs;
  // final data = <OutletModel>[].obs;
  OutletModel? outletModel;
  RxList<DataOutlet?> listOutlet = RxList([]);
  // Map<String, dynamic> listDetail = {};
  Map<String, dynamic> display = {};
  RxString phone = RxString('');
  RxString name = RxString('');
  RxString openAtDay = RxString('');
  RxString openAtTime = RxString('');
  RxString image = RxString('');
  RxString statusName = RxString('');
  RxString address = RxString('');
  RxString googleMaps = RxString('');
  RxInt statusDisabled = 0.obs;
  // RxString openDay = RxString('');
  RxString openTime = RxString('');
  RxString opera = RxString('');
  RxString avatar = RxString('');
  RxString time = RxString('');
  RxString nameStatus = RxString('');
  RxString putData = RxString('');
  RxList<OperationalHour> list = RxList([]);

  // ? initiation dio
  Dio dio = Dio();

  @override
  void onInit() {
    Future.wait([getDataUser()]);
    Future.wait([fetchJson()]);
    // Future.wait([getJson()]);
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

  Future<void> fetchJson() async {
    final String outletResponse =
        await rootBundle.loadString('assets/details.json');
    final getData = await json.decode(outletResponse);
     final OutletDetail dataResponses = OutletDetail.fromJson(getData);
    phone.value = getData["data"]["phone_number"];
    // Map<String, dynamic> listData = getData["data"];
    name.value = getData["data"]["name"];
    statusName.value = getData["data"]["status_name"];
    statusDisabled.value = getData["data"]["status_disabled"];
    openAtDay.value = getData["data"]["open_at_day"];
    openAtTime.value = getData["data"]["open_at_time"];
    address.value = getData["data"]["address"];
    googleMaps.value = getData["data"]["google_maps_url"];
    image.value = getData["data"]["image"];
    putData.value = dataResponses.dataDetail!.address;
    list.value = dataResponses.dataDetail!.operationalHour;
    log('list=${list.length.toString()}');
      // openDay.value = getData["data"]["operational_hour"][0]["open_day"];
      openTime.value = getData["data"]["operational_hour"][0]["open_time"];

    // log('open=${openDay.toString()}');
    name.value = getData["data"]["name"];
    display = getData["data"];
    //  listDetail = getData.dataDetail;
    // debugPrint(outletResponse.toString());
  }

  // Future<void> getJson() async {
  //   final String nowResponse =
  //       await rootBundle.loadString('assets/outlet.json');
  //   final getOutlet = await json.decode(nowResponse);
  //   // list.value = getOutlet["data"];
  //   opera.value = getOutlet["data"][0]["name"];
  //   avatar.value = getOutlet["data"][1]["image_thumb"];
  //   time.value = getOutlet["data"][0]["open_at_display"];
  //   nameStatus.value = getOutlet["data"][0]["status_name"];
  // }

  Future<List<OutletModel?>?> getDataUser() async {
    isLoading(true);
    try {
      // ? syntax Get from Json file online
      final result = await ApiClient().getData(ApiConst.path);
      // ? syntax from get json to model class data
      final OutletModel dataResponse = OutletModel.fromJson(result);
      // ? syntax save value data response in list data
      listOutlet.value = dataResponse.data;
      // debugPrint(result.toString());
      // debugPrint('${listOutlet.length}');

      // log('${listData.length}');

      //  final List req = result["data"];
      isLoading(false);
      // isError(false);

      //  log(req.toString());
      // data.value = req.map((e) => ResponseModel.fromJson(e)).toList();
      // return data;

    } catch (e) {
      isError(true);
      errorMessage(e.toString());
    }
    return null;
  }
}
