import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_row_column.dart';
import 'package:uiprojectbo/controller/outlet_controller.dart';
import 'package:uiprojectbo/outlet/Widget/outlet_details_widget.dart';
import '../../frame/frame_scaffold.dart';
import '../../frame_view/frame_action_wrapper.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';
import '../Widget/dialog_container_outlet_widget.dart';

class OutletDetails extends StatelessWidget {
  OutletDetails({super.key});
  final outletController = Get.put(OutletController());

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return FrameScaffold(
      colorScaffold: AppColors.darkBackground,
      isImplyLeading: false,
      elevation: 0,
      isUseLeading: false,
      action: _actionWrapper(context),
      view: _wrapAll(context),
    );
  }

  Widget _actionWrapper(BuildContext context) {
    return FrameActionWrapper(
        title: 'Black Owl Location',
        fontWeight: FontWeight.bold,
        onPressed: () {
          Get.back();
        });
  }

  /// Widget wrap
  Widget _wrapAll(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: AppColors.darkBackground,
        width: SizeConfig.screenWidth,
        child: ResponsiveRowColumn(
          layout: ResponsiveRowColumnType.COLUMN,
          children: <ResponsiveRowColumnItem>[
            ResponsiveRowColumnItem(child: imageDetail(context)),
            ResponsiveRowColumnItem(child: openOutlet(context)),
            ResponsiveRowColumnItem(
              child: SingleChildScrollView(
                child: dialog(context),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Widget Image Detail Outlet
  Widget imageDetail(BuildContext context) {
    return Container(
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(50),
      decoration: BoxDecoration(
        color: AppColors.greenInfo,
        image: DecorationImage(
            image: NetworkImage('${outletController.image}'),
            fit: BoxFit.cover),
      ),
    );
  }

  /// Widget Open Outlet
  Widget openOutlet(
    BuildContext context,
  ) {
    return Obx((() {
      return OutletDetailsWidget(
          name: '${outletController.name}',
          openAtDay: '${outletController.openAtDay}',
          openAtTime: '${outletController.openAtTime}',
          statusName: '${outletController.statusName}',
          statusDisabled: outletController.statusDisabled.value,
          phoneNumber: '${outletController.phone}');
    }));
  }

  Widget dialog(BuildContext context) {
    return DialogContainer(
      address: '${outletController.address}',
      googleMaps: '${outletController.googleMaps}',
      operationalHour: outletController.list[5].openDay,
      time: outletController.list[0].openTime,
    );
  }
}
