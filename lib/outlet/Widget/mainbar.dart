import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import '../../utils/app_colors.dart';

class MainBar extends StatelessWidget {
  const MainBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.all(2),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            IconButton(
                onPressed: () {
                  Get.back();
                }, icon: Image.asset('assets/icons/Back.png')),
            Text(
              'Black Owl Locations',
              style: GoogleFonts.montserrat(
                  color: AppColors.whiteBackground,
                  fontWeight: FontWeight.w700,
                  fontSize: 18),
            ),
          ],
        ),
        IconButton(onPressed: null, icon: Image.asset('assets/images/logo_bo.png',),iconSize: 50,)
      ],
    ),
  );
  }
}