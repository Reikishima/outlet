import 'package:flutter/material.dart';

// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';
import 'package:uiprojectbo/controller/outlet_controller.dart';
import '../views/outlet_details_views.dart';
import 'custom_card_outlet_widget.dart';

class ValueOutlet extends StatelessWidget {
  final OutletController outletController = Get.put(OutletController());
  ValueOutlet({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (outletController.isLoading.value) {
        return Shimmer.fromColors(
          baseColor: Colors.grey[400]!,
          highlightColor: Colors.grey[300]!,
          child: ListView.builder(
            itemCount: outletController.listOutlet.length,
            itemBuilder: (context, index) => Container(
              margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.white,
              ),
              height: 100,
              width: 200,
            ),
          ),
        );
      } else {
        return ListView.builder(
          itemCount: outletController.listOutlet.length,
          itemBuilder: (context, index) => InkWell(
            onTap: () {
              Get.to(OutletDetails());
            },
            child: CustomCard(
              title: ' ${outletController.listOutlet[index]?.name}',
              image: '${outletController.listOutlet[index]?.imageThumb}',
              time: '${outletController.listOutlet[index]?.openAtDisplay}',
              schedule: '${outletController.listOutlet[index]?.statusName}',
              status: outletController.listOutlet[index]?.statusDisabled,
            ),
          ),
        );
      }
    });
  }
}

// if (outletController.isLoading.value) {
//           return Shimmer.fromColors(
//             baseColor: Colors.grey[400]!,
//             highlightColor: Colors.grey[300]!,
//             child: ListView.builder(
//               itemCount: outletController.listOutlet.length,
//               itemBuilder: (context, index) => Container(
//                 margin:
//                     const EdgeInsets.symmetric(vertical: 10, horizontal: 10),
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(10),
//                   color: Colors.white,
//                 ),
//                 height: 100,
//                 width: 200,
//               ),
//             ),
//           );
//         } else {
//           return ListView.builder(
//             itemCount: outletController.listOutlet.length,
//             itemBuilder: (context, index) => InkWell(
//               onTap: () {
//                 Get.to(OutletDetails());
//               },
//               child: CustomCard(
//                 title: ' ${outletController.opera}',
//                 image: '${outletController.listOutlet[index]?.imageThumb}',
//                 time: '${outletController.listOutlet[index]?.openAtDisplay}',
//                 schedule: '${outletController.listOutlet[index]?.statusName}',
//                 status: outletController.listOutlet[index]?.statusDisabled,
//               ),
//             ),
//           );
//         }

//   Future<ResponseModel?> getDataUser() async {
//     Uri url = Uri.parse("https://reqres.in/api/users/2");
//     var response = await http.get(url);

//     // print(response.statusCode);
//     if (response.statusCode == 200) {
//       // print(response.body);
//       Map<String, dynamic> data =
//           (json.decode(response.body) as Map<String, dynamic>);
//       return ResponseModel.fromJson(data);
//     }
//     return null;
//   }

//   @override
//   Widget build(BuildContext context) {
//     SizeConfig().init(context);
//     return FutureBuilder<ResponseModel?>(
//       future: getDataUser(),
//       builder: (context, snapshot) {
//         if (snapshot.connectionState == ConnectionState.waiting) {
//           return  Center(child: CircularProgressIndicator(
//             color: AppColors.darkBackground,
//           ));
//         } else {
//           if (snapshot.hasData) {
//             return ListView(
//               children: [
//               for (var a = 0; a < 2; a++)
//                 InkWell(
//                   splashColor: AppColors.darkBackground,
//                   highlightColor: Colors.transparent,
//                   onTap: () {
//                     Get.to(const OutletDetails());
//                   },
//                   child: CustomCard(
//                       title: '${snapshot.data!.data!.firstName}',
//                       image: 'assets/images/Black_owl.png',
//                       time: '${snapshot.data!.data!.email}',
//                       schedule: '${snapshot.data!.data!.lastName}'),
//                 ),
//             ]);
//           } else {
//             return const Center(child: Text("TIDAK ADA DATA"));
//           }
//         }
//       },
//     );
//   }
// }
// for (var widget in list)
//                   InkWell(
//                       splashColor: AppColors.darkBackground,
//                       highlightColor: Colors.transparent,
//                       onTap: () {
//                         Get.to(const OutletDetails());
//                       },
//                       child: widget),






