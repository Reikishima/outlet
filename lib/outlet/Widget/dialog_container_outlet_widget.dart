import 'dart:convert';
import 'dart:developer';
import 'dart:math';
import 'package:get/get.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:uiprojectbo/controller/outlet_controller.dart';
import 'package:uiprojectbo/outlet/views/outlet_details_views.dart';
import '../../model/outlet_details_model.dart';
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';

class DialogContainer extends StatelessWidget {
  const DialogContainer(
      {super.key,
      required this.address,
      required this.googleMaps,
      required this.operationalHour,
      required this.time});

  final String address;
  final String googleMaps;
  final String operationalHour;
  final String time;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.horizontal(2.5)),
      margin: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(2),
          horizontal: SizeConfig.horizontal(2.5)),
      // height: SizeConfig.vertical(45),
      decoration: BoxDecoration(
          color: AppColors.lightGold, borderRadius: BorderRadius.circular(10)),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Operational Hours',
            style: GoogleFonts.montserrat(
                color: AppColors.darkBackground,
                fontSize: 14,
                fontWeight: FontWeight.bold),
          ),
          _dialogTable(context, operationalHour, time),
          _wrapTextandMap(context, googleMaps, address)
        ],
      ),
    );
  }
}

/// Widget table Dialog with table
Widget _dialogTable(BuildContext context, String operationalHour, String time) {
  OutletController outletController = Get.put(OutletController());
  // ignore: no_leading_underscores_for_local_identifiers
  DataRow _results(index, data, list) {
    return DataRow(cells: <DataCell>[
      DataCell(
        Text(
          '$data',
        ),
      ),
      DataCell(
        Text(
          '$list',
          style: GoogleFonts.montserrat(
              color: AppColors.darkBackground,
              fontSize: 12,
              fontWeight: FontWeight.bold),
        ),
      ),
    ]);
  }

  return SizedBox(
    width: SizeConfig.screenWidth,
    height: SizeConfig.vertical(30),
    child: ListView.builder(
      itemCount: 1,
      physics: const NeverScrollableScrollPhysics(),
      itemBuilder: ((context, index) {
        return DataTable(
          border: TableBorder.all(
            width: 1.5,
            borderRadius: BorderRadius.circular(10),
          ),
          horizontalMargin: 5,
          dataRowHeight: 30,
          headingRowHeight: 1,
          columns: const <DataColumn>[
            DataColumn(
              label: Text(''),
            ),
            DataColumn(
              label: Text(''),
            ),
          ],
          rows: List.generate(
            outletController.list.length,
            (index) => _results(index, outletController.list[index].openDay, outletController.list[index].openTime),
          ),
        );
      }),
    ),
  );
}

// List <DataColumn> _createColumn(){
//   return [
//     DataColumn(label: Text('Operational Hours',style: GoogleFonts.montserrat(
//       color: AppColors.darkBackground,fontSize: 12,fontWeight: FontWeight.bold
//     ),))
//   ];
// }

// List <DataRow> _createRows(){
//     OutletController outletController = Get.put(OutletController());
//   return DataRow(cells: [
//     DataCell(Text(outletController.list[0].openDay))
//   ]);
// }

/// Widget wrap Address and textAddress
Widget _wrapTextandMap(
    BuildContext context, String googleMaps, String address) {
  return SizedBox(
    width: SizeConfig.horizontal(90),
    height: SizeConfig.vertical(15),
    child: Column(
      children: [
        _addressMap(context, googleMaps),
        _textAddress(context, address)
      ],
    ),
  );
}

/// Widget row Address on Dialog
Widget _addressMap(BuildContext context, String googleMaps) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Text(
        'Address',
        style: GoogleFonts.montserrat(
            color: AppColors.darkBackground,
            fontSize: 14,
            fontWeight: FontWeight.bold),
      ),
      SizedBox(
        width: SizeConfig.horizontal(27),
        height: SizeConfig.vertical(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            InkWell(
              onTap: () {
                googleMaps;
              },
              child: Text(
                'View in Map',
                style: GoogleFonts.montserrat(
                    decoration: TextDecoration.underline,
                    decorationColor: AppColors.darkBackground,
                    decorationStyle: TextDecorationStyle.solid,
                    decorationThickness: 2,
                    color: AppColors.darkBackground,
                    fontSize: 12,
                    fontWeight: FontWeight.w300),
              ),
            ),
            Icon(
              Icons.location_on,
              color: AppColors.darkBackground,
            )
          ],
        ),
      )
    ],
  );
}

/// Widget Text Address Dialog
Widget _textAddress(BuildContext context, String address) {
  return Row(
    children: [
      Flexible(
        child: SizedBox(
          width: SizeConfig.horizontal(85),
          height: SizeConfig.vertical(10),
          child: Text(
            address,
            maxLines: 4,
            style: TextStyle(
              color: AppColors.darkBackground,
              fontWeight: FontWeight.w300,
            ),
          ),
        ),
      )
    ],
  );
}





// Table(
//           border: TableBorder.all(
//               borderRadius: BorderRadius.circular(10), width: 1),
//           columnWidths: const <int, TableColumnWidth>{0: FlexColumnWidth(1.5)},
//           children:[
//             for(var item in items)
//             TableRow(
//               children: [
//                 TableCell(
//                   child: Padding(
//                     padding: EdgeInsets.only(
//                         left: SizeConfig.horizontal(2),
//                         top: SizeConfig.vertical(0.5),
//                         bottom: SizeConfig.vertical(1)),
//                     child: Text(
//                     item,
//                       style: GoogleFonts.montserrat(
//                           color: AppColors.darkBackground,
//                           fontSize: 12,
//                           fontWeight: FontWeight.w300),
//                     ),
//                   ),
//                 ),
//                 TableCell(
//                   child: Padding(
//                     padding: const EdgeInsets.all(3.0),
//                     child: Text(
//                       time,
//                       textAlign: TextAlign.center,
//                       style: GoogleFonts.montserrat(
//                           color: AppColors.darkBackground,
//                           fontSize: 12,
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ],
//         );








// class TableDialog extends StatefulWidget {
//   const TableDialog({super.key});

//   @override
//   State<TableDialog> createState() => _TableDialogState();
// }

// class _TableDialogState extends State<TableDialog> {
//   List _items = [];

//   Future readJson() async {
//     final String response = await rootBundle.loadString('assets/data.json');
//     final data = await json.decode(response);
//     setState(() {
//       _items = data["week"];
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: readJson(),
//       builder: (context, snapshot) {
//         return Table(
//           border: TableBorder.all(
//               borderRadius: BorderRadius.circular(10), width: 1.5),
//           columnWidths: const <int, TableColumnWidth>{
//             0: FlexColumnWidth(1.5),
//           },
//           children: [
//             for (var a = 0; a < 7; a++)
//               // for (var item in list)
//               //   for (var items in hours)
//               TableRow(
//                 children: [
//                   Padding(
//                     padding: EdgeInsets.only(
//                         left: SizeConfig.horizontal(2),
//                         top: SizeConfig.vertical(0.5),
//                         bottom: SizeConfig.vertical(1)),
//                     child: Text(
//                        '${snapshot.data}',
//                       style: GoogleFonts.montserrat(
//                           color: AppColors.darkBackground,
//                           fontSize: 12,
//                           fontWeight: FontWeight.w300),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.all(3.0),
//                     child: Text(
//                       '${snapshot.data}',
//                       textAlign: TextAlign.center,
//                       style: GoogleFonts.montserrat(
//                           color: AppColors.darkBackground,
//                           fontSize: 12,
//                           fontWeight: FontWeight.bold),
//                     ),
//                   ),
//                 ],
//               ),
//           ],
//         );
//       },
//     );
//   }
// }





