import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
// ignore: depend_on_referenced_packages
import '../../utils/app_colors.dart';
import '../../utils/size_config.dart';

class OutletDetailsWidget extends StatelessWidget {
  const OutletDetailsWidget(
      {super.key,
      required this.name,
      required this.openAtDay,
      required this.openAtTime,
      required this.statusName,
      required this.statusDisabled,
      required this.phoneNumber,});

  final String name;
  final String openAtDay;
  final String openAtTime;
  final String statusName;
  final int statusDisabled;
  final String phoneNumber;
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: SizeConfig.vertical(1),
          horizontal: SizeConfig.horizontal(2.5)),
      width: SizeConfig.screenWidth,
      height: SizeConfig.vertical(12),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [_textColumn(context), buttonOutlet(context)],
      ),
    );
  }

  /// Widget text Column Open Outlet
  Widget _textColumn(BuildContext context) {
    return Flexible(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            name,
            style: GoogleFonts.montserrat(
                color: AppColors.whiteBackground,
                fontWeight: FontWeight.bold,
                fontSize: 14),
          ),
          Flexible(
            child: RichText(
              text: TextSpan(
                text: openAtDay,
                style: GoogleFonts.montserrat(
                    color: AppColors.whiteBackground,
                    fontSize: 12,
                    fontWeight: FontWeight.bold),
                children: <TextSpan>[
                  TextSpan(
                    text: openAtTime,
                    style: GoogleFonts.montserrat(
                        color: AppColors.whiteBackground,
                        fontSize: 12,
                        fontWeight: FontWeight.w300),
                  )
                ],
              ),
            ),
          ),
          Flexible(
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: SizeConfig.horizontal(3),
              ),
              margin: EdgeInsets.only(
                  bottom: SizeConfig.vertical(0.5),
                  top: SizeConfig.vertical(1)),
              decoration: BoxDecoration(
                color: statusDisabled == 0
                    ? AppColors.lightGold
                    : AppColors.greyDisabled,
                borderRadius: BorderRadius.circular(20),
              ),
              height: SizeConfig.vertical(4),
              child: Text(
                statusName,
                textAlign: TextAlign.center,
                style: GoogleFonts.montserrat(
                    color: AppColors.darkBackground,
                    fontSize: 14,
                    fontStyle: FontStyle.italic),
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Widget Button Open Outlet
  Widget buttonOutlet(BuildContext context) {
    return SizedBox(
      height: SizeConfig.vertical(6),
      child: TextButton.icon(
        style: TextButton.styleFrom(
            backgroundColor: AppColors.darkBackground,
            foregroundColor: AppColors.darkBackground,
            shape: RoundedRectangleBorder(
              side: BorderSide(width: 1, color: AppColors.lightGold),
              borderRadius: BorderRadius.circular(10),
            )),
        onPressed: () {},
        icon: Icon(
          Icons.call,
          size: 20,
          color: AppColors.lightGold,
        ),
        label: Text(
          phoneNumber,
          style:
              GoogleFonts.montserrat(color: AppColors.lightGold, fontSize: 12),
        ),
      ),
    );
  }
}
