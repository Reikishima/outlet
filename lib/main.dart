import 'package:flutter/material.dart';
// ignore: depend_on_referenced_packages
import 'package:get/get.dart';
import 'package:responsive_framework/responsive_framework.dart';

import 'outlet/views/outlet_views.dart';

void main() {
  runApp(
     GetMaterialApp(
      //Responsive ScreenGit
       builder: (BuildContext context, child) => ResponsiveWrapper.builder(
        BouncingScrollWrapper.builder(context, child!),
        minWidth: 375,
        maxWidth: 812,
        breakpoints: const <ResponsiveBreakpoint>[
          ResponsiveBreakpoint.autoScale(375, name: MOBILE),
        ],
      ),
      home: const Outlet(),
    ),
  );
}