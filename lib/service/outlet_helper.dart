class ApiConst {
  ApiConst._();
  static const String baseUrl = "https://training-test-rafi.free.beeceptor.com/my/api/";
  static const String path = "outlet-list";
  // static const String baseUrlLocal = "assets/";
  // static const String paths = "details.json";
  // static const String baseUrl = "https://test-json.free.beeceptor.com/test/";
  // static const String path = "training";
  // static const String baseUrl = "https://reqres.in/api/";
  // static const String path = "users?page";
}
