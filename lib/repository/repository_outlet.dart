import 'package:dio/dio.dart';
import '../service/outlet_helper.dart';

class ApiClient {
  // ? get data from Json from Url to dart by dio
  Future getData(String path) async {
    try {
      final response =
          await Dio(BaseOptions(baseUrl: ApiConst.baseUrl)).get(path);
      return response.data;
    } on DioError catch (e) {
      throw Exception(e.message);
    }
  }
}

// class ApiClientSecond {
//   // ? get data from Json from Url to dart by dio
//   Future getData(String paths) async {
//     try {
//       final response =
//           await Dio(BaseOptions(baseUrl: ApiConst.baseUrlLocal)).get(paths);
//       return response.data;
//     } on DioError catch (e) {
//       throw Exception(e.message);
//     }
//   }
// }